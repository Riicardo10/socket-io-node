const {io} = require( '../server' );

io.on( 'connection', (client) => {
    console.log( 'Se conecto un usuario!!!' );
    
    client.on( 'disconnect', () =>{
        console.log( 'Usuario desconectado' );
    } );

    // enviamos un EMIT al CLIENTE
    client.emit( 'enviarMensaje', {
        usuario: 'Admin',
        mensaje: 'Bienvenido a esta app!'
    } );

    // obtenemos el EMIT del CLIENTE
    client.on( 'enviarMensaje', (data, callback) => {
        console.log( 'Cliente: ', data );
        client.broadcast.emit( 'enviarMensaje', data );
    
        /*if(mensaje.usuario) {
            callback( {
                res: 'Todo salio bien :)'
            } );
        }
        else{
            callback( {
                res: 'Algo salio mal :('
            } );
        }*/
    } );
} );


