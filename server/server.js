const express = require( 'express' );
const app = express();
const path = require( 'path' );
const socketIO = require( 'socket.io' );
const http = require( 'http' );

let server = http.createServer( app );

const publicPath = path.resolve( __dirname, '../public' );
const PORT = process.env.PORT || 3000;
app.use(express.static( publicPath ));

// conexion del backend
module.exports.io = socketIO( server );

require( './sockets/socket.js' );

server.listen( PORT, err => {
    if (err) throw new Error(err);
    console.log( 'Servidor corriendo en puerto: ' + PORT );
});