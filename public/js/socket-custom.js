var socket = io();
// cuando se conectan
// ON: ESCUCHAN SUCESOS
socket.on( 'connect', function() {
    console.log( 'Conectado al servidor' );
} );
// cuando se desconectan
socket.on( 'disconnect', function() {
    console.log( 'Desconectado del servidor' );
} );
// enviamos un EMIT al SERVIDOR
socket.emit( 'enviarMensaje', {
    usuario: 'Ricardo Flores',
    mensaje: 'Hola mundo ;)'
}, function( res ) {
    console.log( 'Respuesta: ', res );
} );
// obtenemos el EMIT que manda el SERVIDOR
socket.on( 'enviarMensaje', function(mensaje){
    console.log( 'Servidor: ', mensaje );
    //alert(mensaje.usuario + ' -> ' + mensaje.mensaje)
} ); 
